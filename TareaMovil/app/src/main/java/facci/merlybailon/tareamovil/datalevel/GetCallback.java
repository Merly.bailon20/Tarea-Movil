package facci.merlybailon.tareamovil.datalevel;


public interface GetCallback<DataObject> {

    public void done(DataObject object, DataException e);

}
