package facci.merlybailon.tareamovil.datalevel;

import java.util.ArrayList;


public interface FindCallback<DataObject> {
    public void done(ArrayList<DataObject> objects, DataException e);



}
